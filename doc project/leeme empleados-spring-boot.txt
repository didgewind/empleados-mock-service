Proyecto spring boot finchley para probar despliegues en openshift. Origin apunta a un repositorio
bitbucket, hay otro project en bitbucket llamado empleados-base de donde los alumnos pueden hacer
forks para trabajar con openshift en sus equipos (o en el sandbox de red hat).

El puerto de escucha es el 8080 por simplificar.

* empleados-mock-service: microservicio rest de empleados eliminando referencias a bd,
a seguridad y a eureka para poder ejecutar en cualquier equipo. A partir de un commit hay un ejemplo de swagger.
También tenemos una clase de configuración para permitir CORS y así poder atacarlo desde una app angular.
Usa i18n para el mensaje de bienvenida. Hemos añadido una línea en el .properties para poder lanzar
diferentes instancias del servicio y probar balance de carga. Para lanzar diferentes instancias simplemente
hay que ejecutar la app, porque el puerto está configurado para que se elija aleatoriamente.

	- Dos daos - uno en memoria, el otro inmutable que siempre devuelve el mismo empleado
	  
	- Una clase java config para instanciar DAODumb.
	
	- Un SwaggerConfig para configurar Swagger (url de acceso: http://localhost:8080/swagger-ui.html)
	  
	- Un controlador Rest que funciona con JSON y XML y mapea a /empleados/ y sus derivados. Si probamos
	  directamente con el navegador puede devolver xml. El Json
	  se puede probar con Postman. Su api es:
	
		- GET /empleados/: devuelve todos los empleados
		- GET /empleados/x: devuelve el empleado con cif x
		- PUT /empleados/x: modifica un empleado (sólo admin)
		- DELETE /empleados/x: elimina el empleado con cif x (sólo admin)
		- POST /empleados/: inserta un empleado (sólo admin)

	  Trabaja con Json y xml porque Spring detecta las librerías Jackson en el path y 
	  utiliza httpmessageconverters. Es por ello que al hacer la misma petición REST 
	  con un navegador web y Postman el segundo devuelve siempre JSON pero el primero puede devolver xml
	  (depende de la cabecera de aceptación de respuesta que envía el cliente).
	  
	  Devuelve cabeceras con código de error en caso de que se produzcan (por ejemplo, NOT_FOUND en caso
	  de delete o get de un cif que no existe, o CONFLICT si el empleado está repetido)

	- EmpleadosApp arranca el servidor embebido.


